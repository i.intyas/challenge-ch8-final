import "bootstrap/dist/css/bootstrap.min.css";
import { useState } from "react";
import { uid } from "uid";
import Filter from "./component/Filter";
import List from "./component/List";

const App = () => {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [level, setLevel] = useState("");
  const [experience, setExperience] = useState("");
  const [dataPlayer, setDataPlayer] = useState([]);
  const [dataFilter, setDataFilter] = useState([]);
  const [isEdit, setIsEdit] = useState({ id: null, status: false });

  const handlerClick = (e) => {
    e.preventDefault();

    //data inputan dari form yang digabung jadi satu  inputDataPlayer
    const inputDataPlayer = {
      id: uid(),
      namaUser: userName,
      emailUser: email,
      passwordUser: password,
      experienceUser: level,
      levelUser: experience,
    };

    if (isEdit.status) {
      dataPlayer.forEach((el) => {
        if (el.id === isEdit.id) {
          el.namaUser = userName;
          el.emailUser = email;
          el.passwordUser = password;
          el.experienceUser = level;
          el.levelUser = experience;
        }
      });
    } else {
      const allDataPlayers = [...dataPlayer, inputDataPlayer];
      setDataPlayer(allDataPlayers);
      setDataFilter(allDataPlayers);
    }

    setIsEdit({ id: null, status: false });
    setUserName("");
    setEmail("");
    setPassword("");
    setExperience("");
    setLevel("");
  };

  const handlerEdit = (id) => {
    setIsEdit({ id: id, status: true });
    const isPlayerExist = dataPlayer.find((el) => el.id === id);
    if (isPlayerExist) {
      setUserName(isPlayerExist.namaUser);
      setEmail(isPlayerExist.emailUser);
      setPassword(isPlayerExist.passwordUser);
      setExperience(isPlayerExist.experienceUser);
      setLevel(isPlayerExist.levelUser);
    }
  };

  const handlerFilter = (params) => {
    console.log("isi params", params);

    const targetUsername = params.namaUser;
    const targetEmail = params.emailUser;
    const targetExperience = params.experienceUser;
    const targetLevel = params.levelUser;

    if (
      targetUsername === "" &&
      targetEmail === "" &&
      targetExperience === "" &&
      targetLevel === ""
    ) {
      setDataFilter(dataPlayer);
      return true;
    }

    const newFilteredPlayers = dataPlayer.filter((el) => {
      if (targetUsername && el.namaUser !== targetUsername) return false;
      if (targetEmail && el.emailUser !== targetEmail) return false;
      if (targetExperience && el.experienceUser !== targetExperience)
        return false;
      if (targetLevel && el.levelUser !== targetLevel) return false;
      return true;
    });
    setDataFilter(newFilteredPlayers);
  };

  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-2 pt-5 ">
            <h5>Create New User </h5>
            <div className="mb-3">
              <label className="form-label">Username</label>
              <input
                type="text"
                name="username"
                className="form-control"
                placeholder="ex: JohnDoe123"
                value={userName}
                onChange={(e) => setUserName(e.target.value)}
              />
            </div>
            <div className="mb-3">
              <label className="form-label">email</label>
              <input
                className="form-control"
                name="email"
                type="email"
                placeholder="ex:email@email.com"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              ></input>
            </div>
            <div className="mb-3">
              <label className="form-label">password</label>
              <input
                className="form-control"
                name="password"
                placeholder="ex: 12345"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              ></input>
            </div>
            <div className="mb-3">
              <label className="form-label">experience</label>
              <input
                className="form-control"
                name="password"
                placeholder="ex: 1"
                value={experience}
                onChange={(e) => setExperience(e.target.value)}
              ></input>
            </div>
            <div className="mb-3">
              <label className="form-label">level</label>
              <input
                className="form-control"
                name="password"
                placeholder="ex: 1"
                value={level}
                onChange={(e) => setLevel(e.target.value)}
              ></input>
            </div>
            <button
              type="submit"
              className="btn btn-primary text-dark"
              onClick={handlerClick}
              disabled={!userName || !email || !password}
            >
              Submit
            </button>
          </div>
          <List dataFilter={dataFilter} onEdit={handlerEdit} />
          <Filter onFilter={handlerFilter} />
        </div>
      </div>
    </>
  );
};

export default App;
