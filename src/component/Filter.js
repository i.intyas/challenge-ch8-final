import React, { useState } from "react";

export default function Filter(props) {
  const [filterUserName, setFilterUserName] = useState("");
  const [filterEmail, setFilterEmail] = useState("");
  const [filterLevel, setFilterLevel] = useState("");
  const [filterExperience, setFilterExperience] = useState("");

  const fields = {
    namaUser: filterUserName,
    emailUser: filterEmail,
    experienceUser: filterExperience,
    levelUser: filterLevel,
  };

  console.log("isi form fields", fields);

  return (
    <div className="col-2 pt-5 ">
      <h5>Search User</h5>
      <div className="mb-3">
        <label className="form-label">Username</label>
        <input
          type="text"
          name="username"
          className="form-control"
          placeholder="ex: JohnDoe123"
          value={filterUserName}
          onChange={(e) => setFilterUserName(e.target.value)}
        />
      </div>
      <div className="mb-3">
        <label className="form-label">email</label>
        <input
          className="form-control"
          name="email"
          type="email"
          placeholder="ex:email@email.com"
          value={filterEmail}
          onChange={(e) => setFilterEmail(e.target.value)}
        ></input>
      </div>
      <div className="mb-3">
        <label className="form-label">experience</label>
        <input
          className="form-control"
          name="password"
          placeholder="ex: 1"
          value={filterExperience}
          onChange={(e) => setFilterExperience(e.target.value)}
        ></input>
      </div>
      <div className="mb-3">
        <label className="form-label">level</label>
        <input
          className="form-control"
          name="password"
          placeholder="ex: 1"
          value={filterLevel}
          onChange={(e) => setFilterLevel(e.target.value)}
        ></input>
      </div>

      <button
        type="submit"
        className="btn btn-primary text-dark ms-1"
        onClick={() => props.onFilter(fields)}
      >
        Search
      </button>
    </div>
  );
}
