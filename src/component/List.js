export default function List({ dataFilter, onEdit }) {
  return (
    <div className="col-8">
      <table className="table mt-5">
        <thead>
          <tr>
            <th scope="col">Id</th>
            <th scope="col">USERNAME</th>
            <th scope="col">EMAIL</th>
            <th scope="col">PASSWORD</th>
            <th scope="col">EXPERIENCE</th>
            <th scope="col">LEVEL</th>
            <th scope="col">ACTION</th>
          </tr>
        </thead>

        {dataFilter.map((el, i) => {
          return (
            <tbody key={i}>
              <tr>
                <td>{i + 1}</td>
                <td>{el.namaUser}</td>
                <td>{el.emailUser}</td>
                <td>{el.passwordUser}</td>
                <td>{el.experienceUser}</td>
                <td>{el.levelUser}</td>
                <button
                  type="submit"
                  className="btn btn-warning"
                  onClick={() => onEdit(el.id)}
                >
                  EDIT
                </button>
              </tr>
            </tbody>
          );
        })}
      </table>
    </div>
  );
}
